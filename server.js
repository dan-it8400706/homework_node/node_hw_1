// eslint-disable-next-line no-undef
const http = require('http');
// eslint-disable-next-line no-undef
const port = process.argv[2] || 1337;
let requestCount = 0;

const server = http.createServer((req, res) => {
  requestCount++;
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify({
    message: 'Request handled successfully',
    requestCount,
  }));
});

server.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

// eslint-disable-next-line no-undef
process.on('SIGINT', () => {
  server.close();
  console.log('Server closed');
});